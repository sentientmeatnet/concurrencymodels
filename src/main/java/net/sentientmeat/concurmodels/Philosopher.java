/*
 * Work in progress by Dean Elzinga. May not be used
 * for any purpose other than Dean's personal study
 * until it's in decent shape to show to show to
 * other people, which you can determine only by
 * obtaining the express, written permission of the
 * author, who can be reached at agizlecd ta liamg
 * tod moc. (Just reverse each string to reveal its
 * content.)
 */
package net.sentientmeat.concurmodels;

import it.unimi.dsi.util.XorShift128PlusRandom;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author dcelzinga
 * @param <C>
 */
public class Philosopher<C> extends Thread {

  private C left, right;
  private final XorShift128PlusRandom random;
  private static final XorShift128PlusRandom RANDOM = 
      new XorShift128PlusRandom();
  private final String name;
  private static final String[] NAME_IDEAS = new String[]{
    "Aristotle", "Berkeley", "Confucious", "Descartes", "Epicurus", "Frege",
    "Galileo", "Hegel", "Ignatius", "James", "Kant", "Locke", "Marx", 
    "Nietsche", "Origen", "Plato", "Quine", "Russell", "Socrates", "Spinoza",
    "Thales", "Unger", "Voltaire", "Wittgenstein", "Xenophanes", "Yoda", "Zeno"  
  };
  private static AtomicLong COUNTER = new AtomicLong(0);
  private final long id; // immutable
  private static final Logger logger = LoggerFactory.getLogger("Philosopher");
  public Philosopher(C left, C right) {
    this.left = left; this.right = right;
    this.id = Philosopher.COUNTER.getAndIncrement();
    this.random = new XorShift128PlusRandom();
    this.name = NAME_IDEAS[random.nextInt(NAME_IDEAS.length)];
    logger.info("Creating philosopher {}, named {}.", id);
  }
  public void think() {
    
  }
  public void run() {
    try {
      for (;;) {
        logger.info("{} thinking.", name);
        Thread.sleep(random.nextInt(1000));   // Think for a while.
        logger.info("{} reaching for left chopstick, {}.", name, ((Chopstick)left).name);
        synchronized(left) {                  // Grab left chopstick.
          logger.info("{} reaching for right chopstick, {}.", name, ((Chopstick)right).name);
          synchronized(right) {               // Grab right chopstick.
            logger.info("{} eating.", name);
            Thread.sleep(random.nextInt(1000)); // Eat for a while.
          }
        }
      }
    } catch(InterruptedException e) {}
  }

  private static class Chopstick {
    final int id; // Immutable
    static final String[] NAMES = 
    { 
      "ash", "bamboo", "cherry", "dogwood", "elm", "fir", "guava", "hawthorn",
       "ironwood", "juniper", "kauri", "linden", "maple", "neem", "oak",
       "pine", "quandong", "redwood", "spruce", "teak", "umbrella_tree",
       "viburnum", "willow", "xanthoceras", "yew", "zebrawood"
    };
    String name;
    static final AtomicInteger COUNTER = new AtomicInteger(0);
    static final int SIZE_DEFAULT = 1_000;
    double[] measurements;

    Chopstick() {
      this(SIZE_DEFAULT);
    }

    Chopstick(int size) {
      this.id = Chopstick.COUNTER.getAndIncrement();
      this.name = Chopstick.NAMES[id];
      this.measurements = new double[size];
    }
  }
  
  /**
   * Do my best to create a bunch of chopstick-eating, left/right - handed
   * philosophers.
   * 
   * @param args
   */
  @SuppressWarnings("unchecked")
  public static void main(String[] args) {
    Logger logger = LoggerFactory.getLogger(Philosopher.class);
    final int NUM_PHILS = 8;
    /* Initialize chopsticks. */
    Chopstick[] chops = new Chopstick[NUM_PHILS];
    for (int i = 0; i < chops.length; i++) {
      chops[i] = new Chopstick();
    }

    Philosopher<Chopstick>[] phils;
    phils = (Philosopher<Chopstick>[]) new Philosopher[NUM_PHILS];
    for (int i = 0; i < phils.length; i++) {
      phils[i] = new Philosopher<>(chops[i], chops[(i + 1) % NUM_PHILS]);
    }
    for (Philosopher<Chopstick> phil : phils) {
      phil.start();
    }
    for (Philosopher<Chopstick> phil : phils) {
      try {
        phil.join();
      } catch (InterruptedException e) {}
    }  
  }
}
