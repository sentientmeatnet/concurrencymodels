/*
 * Work in progress by Dean Elzinga. May not be used
 * for any purpose other than Dean's personal study
 * until it's in decent shape to show to show to
 * other people, which you can determine only by
 * obtaining the express, written permission of the
 * author, who can be reached at agizlecd ta liamg
 * tod moc. (Just reverse each string to reveal its
 * content.) 
 */
package net.sentientmeat.concurmodels;

/**
 *
 * @author dcelzinga
 */
public class Counting {
  
  
  
  public static void main(String[] args) throws InterruptedException {
    class Counter {
      private int count = 0;
      public synchronized void increment() { ++count; }
      public int getCount() { return count; }
    }
    final Counter counter = new Counter();
    class CountingThread extends Thread {
      public void run() {
        for (int x = 0; x < 10_000; ++x)
          counter.increment();
      }
    }
    
    CountingThread t1 = new CountingThread();
    CountingThread t2 = new CountingThread();
    t1.start(); t2.start();
    t1.join(); t2.join();
    System.out.println(counter.getCount());
  }
}
