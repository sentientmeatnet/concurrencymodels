/*
 * Work in progress by Dean Elzinga. May not be used
 * for any purpose other than Dean's personal study
 * until it's in decent shape to show to show to
 * other people, which you can determine only by
 * obtaining the express, written permission of the
 * author, who can be reached at agizlecd ta liamg
 * tod moc. (Just reverse each string to reveal its
 * content.)
 */
package net.sentientmeat.concurmodels;

import it.unimi.dsi.util.XorShift128PlusRandom;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Demonstrate reentrant locks, this time signaling the neighbors to the left
 * and right rather than synchronizing on the use of the chopstick objects.
 * @author dcelzinga
 * @param <T>
 */
public class PhilosopherCondition<T> extends Thread {
  private static final boolean VERBOSE = true;
  protected T t;
  private boolean eating;
  protected PhilosopherCondition<T> left;
  protected PhilosopherCondition<T> right;
  private ReentrantLock table;
  private Condition condition;
  
  private final XorShift128PlusRandom random;
  private static final XorShift128PlusRandom RANDOM = 
      new XorShift128PlusRandom();
  private final String name;
  private static final String[] NAMES = new String[]{
    "Aristotle", "Berkeley", "Confucious", "Descartes", "Epicurus", "Frege",
    "Galileo", "Hegel", "Ignatius", "James", "Kant", "Locke", "Marx", 
    "Nietsche", "Origen", "Plato", "Quine", "Russell", "Spinoza",
    "Thales", "Unger", "Voltaire", "Wittgenstein", "Xenophanes", "Yoda", "Zeno"  
  };
  private static AtomicInteger idCounter = new AtomicInteger(0);
  private final int id; // immutable
  private static final Logger logger = LoggerFactory.getLogger("Philosopher");

//  public PhilosopherCondition(PhilosopherCondition<T> left, PhilosopherCondition<T> right) {
//    this.left = left; this.right = right;
//    this.id = PhilosopherCondition.COUNTER.getAndIncrement();
//    this.random = new XorShift128PlusRandom();
//    this.name = NAME_IDEAS[random.nextInt(NAME_IDEAS.length)];
//    logger.info("Creating philosopher {}, named {}.", id);
//  }
  PhilosopherCondition(ReentrantLock table) {
    eating = false;
    this.table = table;
    condition = table.newCondition();
    random = new XorShift128PlusRandom();
    id = PhilosopherCondition.idCounter.getAndIncrement();
    name = PhilosopherCondition.NAMES[id % NAMES.length];
  }
  
  public void run() {
    try {
      for (;;) {
        think();
        eat();
      }
    } catch (InterruptedException e) {
    }
  }

  private void think() throws InterruptedException {
    logger.info("{} about to think.", name);
    table.lock();
    try {
      eating = false;
      left.condition.signal();
      right.condition.signal();
    } finally { table.unlock(); }
    Thread.sleep(1000);
    if (VERBOSE) logger.info("{} done thinking.", name);
  }
  private void eat() throws InterruptedException {
    logger.info("{} about to eat.", name);
    table.lock();
    try {
      while (left.eating || right.eating)
        condition.await();
      eating = true;
    } finally { table.unlock(); }
    Thread.sleep(1000);
    if (VERBOSE) logger.info("{} done eating.", name);
  }
  
  /**
   * @return the t
   */
  public T getT() {
    return t;
  }

  /**
   * @param t the t to set
   */
  public void setT(T t) {
    this.t = t;
  }

  /**
   * @return the left
   */
  public PhilosopherCondition<T> getLeft() {
    return left;
  }

  /**
   * @param left the left to set
   */
  public void setLeft(PhilosopherCondition<T> left) {
    this.left = left;
  }

  /**
   * @return the right
   */
  public PhilosopherCondition<T> getRight() {
    return right;
  }

  /**
   * @param right the right to set
   */
  public void setRight(PhilosopherCondition<T> right) {
    this.right = right;
  }

  private static class Chopstick {
    final int id; // Immutable
    static final String[] WOOD_NAMES = 
    { 
      "ash", "bamboo", "cherry", "dogwood", "elm", "fir", "guava", "hawthorn",
       "ironwood", "juniper", "kauri", "linden", "maple", "neem", "oak",
       "pine", "quandong", "redwood", "spruce", "teak", "umbrella_tree",
       "viburnum", "willow", "xanthoceras", "yew", "zebrawood"
    };
    String name;
    static final AtomicInteger COUNTER = new AtomicInteger(0);
    static final int SIZE_DEFAULT = 1_000;
    double[] measurements;

    Chopstick() {
      this(SIZE_DEFAULT);
    }

    Chopstick(int size) {
      this.id = Chopstick.COUNTER.getAndIncrement();
      this.name = Chopstick.WOOD_NAMES[id];
      this.measurements = new double[size];
    }
  }
  
  /**
   * Demonstration simulation of canonical concurrency problem of dining
   * philosophers contending over chopsticks
   * Do my best to create a bunch of chopstick-eating, left/right - handed
   * philosophers.
   * 
   * @param args
   */
  @SuppressWarnings("unchecked")
  public static void main(String[] args) {
    ReentrantLock table = new ReentrantLock();
    Logger mainLogger = LoggerFactory.getLogger(PhilosopherCondition.class);

    final int NUM_PHILS = 8;
    /* Initialize chopsticks. */
    Chopstick[] chops = new Chopstick[NUM_PHILS];
    for (int i = 0; i < chops.length; i++) {
      chops[i] = new Chopstick();
    }

    // Initialize the philosophers.
    PhilosopherCondition<Chopstick>[] phils;
    phils = new PhilosopherCondition[NUM_PHILS];
    for (int i = 0; i < phils.length; i++) {
      phils[i] = new PhilosopherCondition<>(table);
    }
    
    // Set each philosophers' neighbors equal to +1 or -1, mod their number.
    for (int i = 0; i < phils.length; i++) {
      phils[i].setLeft(phils[(i + phils.length - 1) % phils.length]);
      phils[i].setRight(phils[(i + 1) % phils.length]);
    }
    
    // Start the simulation.
    for (PhilosopherCondition<Chopstick> phil : phils) {
      phil.start();
    }
    for (PhilosopherCondition<Chopstick> phil : phils) {
      try {
        phil.join();
      } catch (InterruptedException e) {}
    }  
  }
}
