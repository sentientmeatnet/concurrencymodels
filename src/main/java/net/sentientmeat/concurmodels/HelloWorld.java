package net.sentientmeat.concurmodels;
/**
 * <p>HelloWorld class.</p>
 *
 * @author dcelzinga
 * @version $Id: $Id
 */
public class HelloWorld {
    /**
     * <p>main.</p>
     *
     * @param args an array of {@link java.lang.String} objects.
     * @throws java.lang.InterruptedException if any.
     */
    public static void main(String[] args) throws InterruptedException {
        Thread myThread = new Thread() {
            public void run() {
                System.out.println("Hello from new thread");
            }
        };
        myThread.start();
        Thread.yield();
        System.out.println("Hello from main thread");
        myThread.join();
    }
}
