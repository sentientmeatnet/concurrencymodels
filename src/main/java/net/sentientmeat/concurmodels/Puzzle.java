/*
 * Work in progress by Dean Elzinga. May not be used
 * for any purpose other than Dean's personal study
 * until it's in decent shape to show to show to
 * other people, which you can determine only by
 * obtaining the express, written permission of the
 * author, who can be reached at agizlecd ta liamg
 * tod moc. (Just reverse each string to reveal its
 * content.) 
 */
package net.sentientmeat.concurmodels;

/**
 *
 * @author dcelzinga
 */
public class Puzzle {
  static boolean answerReady = false;
  static int answer = 0;
  static Thread t1 = new Thread() {
    public void run() {
      answer = 42;
      answerReady = true;
    }
  };
  static Thread t2 = new Thread() {
    public void run() {
      if (answerReady)
        System.out.println("The meaning of life is: " + answer);
      else
        System.out.println("I don' know the answer");
    }
  };

  /**
   *
   * @param args
   * @throws InterruptedException
   */
  public static void main(String[] args) throws InterruptedException {
    t1.start(); t2.start();
    t1.join(); t2.join();
  }
}
